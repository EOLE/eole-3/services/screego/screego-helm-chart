# Changelog

### [1.1.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/compare/release/1.1.0...release/1.1.1) (2025-03-04)


### Bug Fixes

* publish v1.2.0 stable version ([28e6ee0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/28e6ee05636ffa94ef04fdd42f9d7b8de65f015d))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/compare/release/1.0.0...release/1.1.0) (2024-09-16)


### Features

* upgrade screego to 1.10.5-eole3.0 ([d0cac95](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/d0cac954e4162da1c41b13aa3a1b55d2d73e2c8e))

## 1.0.0 (2023-12-04)


### Features

* change appversion to 1.8.2 for screego ([df8154e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/df8154ea11824c2273c286449224d5af5fde0a41))
* update appversion to 1.10.0 ([971c791](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/971c791d0997c40ced586e6ff22cc1fd5739cbf2))
* update appversion to 1.10.2 ([6cdafb7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/6cdafb7849b8f2f5442d57f1a0d69a42d82975b9))
* update appversion to 1.8.0 ([69687ed](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/69687ed98a67f56aab36b462f9e16c736dd9c3a0))


### Bug Fixes

* remove image.tag in default values.yaml ([77c55fd](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/77c55fd28e0bd2b6fa72b921f26e0015d658f2e8))
* update app version to 1.8.1 ([1f4361e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/1f4361e53ab06412d1441deb6b99556dc7cf4202))
* update chart.yaml with version numbers ([b32173d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-helm-chart/commit/b32173df722a5712641aada80ac11320d332931f))
